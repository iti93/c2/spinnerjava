package com.example.spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //DECLARAR VARIABLES
    private Spinner spnPaises;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //RELACION DE VARIABLES DECLARADAS CON LAS ID DE XML
        spnPaises = (Spinner) findViewById(R.id.spnPaisesxml);
        ArrayAdapter<String> Adaptador = new
                ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_expandable_list_item_1, getResources().getStringArray(R.array.paises));
        spnPaises.setAdapter(Adaptador);

        //hacer esto
        spnPaises.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                Toast.makeText(MainActivity.this,"Selecciono el pais " + adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();}
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }});}}
